package Telas;

import Basicas.Excecoes.*;
import DAO.RepoPaciente;
import Basicas.Paciente;
import Negocio.NegPaciente;
import javax.swing.JOptionPane;

public class TelaPacienteCadastrar extends javax.swing.JInternalFrame {

    public TelaPacienteCadastrar() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Cadastro de Pacientes");
        setFocusCycleRoot(false);

        jButton1.setText("cadastrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(jButton1)
                .addContainerGap(328, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(jButton1)
                .addContainerGap(248, Short.MAX_VALUE))
        );

        setBounds(262, 50, 500, 500);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Botão Cadastrar
            Paciente p = new Paciente();
            p.getTelefones().add("81-99999-9999");
            p.getTelefones().add("81-97777-7777");
            p.alterar(1, "Nome", "123.456.789-10", "Rua São Bartolomeu", "Jardim Paulista", "Recife", "PE", p.getTelefones());

            NegPaciente neg = new NegPaciente();
            try{
                neg.inserir(p);
            }catch(DadoInvalido e){
                JOptionPane.showMessageDialog(null,e.getErro());
            }catch(CampoVazio e){
                JOptionPane.showMessageDialog(null,e.getErro());
            }catch(RegistroExistente e){
                JOptionPane.showMessageDialog(null,e.getErro());
            }
        
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
