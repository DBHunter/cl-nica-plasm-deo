package Basicas;
import java.util.ArrayList;

public class Paciente {
    //Pessoais
    private String nome, cpf, logradouro, bairro, cidade, estado;
    private ArrayList<String> telefones;
    private int codigo;
    
    public Paciente(){
        this.codigo = -1;
        this.nome = null;
        this.cpf = null;
        this.logradouro = null;
        this.bairro = null;
        this.cidade = null;
        this.estado = null;
        this.telefones = new ArrayList<String>();
    }

    public void alterar(int codigo, String nome, String cpf, String logradouro, String bairro, String cidade, String estado, ArrayList telefones) {
        this.codigo = codigo;
        this.nome = nome;
        this.cpf = cpf;
        this.logradouro = logradouro;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.telefones = telefones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public ArrayList getTelefones(){
        return this.telefones;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
