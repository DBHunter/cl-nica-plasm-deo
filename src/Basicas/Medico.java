package Basicas;
import java.util.ArrayList;

public class Medico {
    //Pessoais
    private int crm;
    private String nome, cpf, logradouro, bairro, cidade, estado;
    private ArrayList<String> telefones;
    
    public Medico(){
        this.crm = -1;
        this.nome = null;
        this.cpf = null;
        this.logradouro = null;
        this.bairro = null;
        this.cidade = null;
        this.estado = null;
        this.telefones = new ArrayList<String>();
    }

    public void alterar(int crm, String nome, String cpf, String logradouro, String bairro, String cidade, String estado, String cartao, int codseguranca, String vencimento, char formapagamento, ArrayList telefones) {
        this.crm = crm;
        this.nome = nome;
        this.cpf = cpf;
        this.logradouro = logradouro;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.telefones = telefones;
    }

    public int getCrm() {
        return crm;
    }

    public void setCrm(int crm) {
        this.crm = crm;
    }
    
    public ArrayList getTelefones(){
        return this.telefones;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
