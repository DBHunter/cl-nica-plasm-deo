
package Basicas;

public class Excecoes {
    
    public static class DadoInvalido extends Exception{
        String erro;
        public DadoInvalido(String msg){
            super(msg);
            this.erro = msg;
	}

        public String getErro() {
            return erro;
        }
        
    }
    public static class CampoVazio extends Exception{
        String erro;
        public CampoVazio(String msg){
            super(msg);
            this.erro = msg;
	}

        public String getErro() {
            return erro;
        }
    }
    public static class RegistroExistente extends Exception{
        String erro;
        public RegistroExistente(String msg){
            super(msg);
            this.erro = msg;
	}

        public String getErro() {
            return erro;
        }
    } 
        public static class RegistroInexistente extends Exception{
        String erro;
        public RegistroInexistente(String msg){
            super(msg);
            this.erro = msg;
	}

        public String getErro() {
            return erro;
        }
    } 
}
