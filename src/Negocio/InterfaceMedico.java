package Negocio;
import Basicas.Excecoes;
import Basicas.Paciente;

public interface InterfaceMedico {
    
    public void inserir(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente;
    public void alterar(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente;
    public void remover(String cod);
    public void historico(String cod);
    public Paciente buscarPorNome(String nome)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
    public Paciente buscarPorCpf(String cpf)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
    public Paciente buscarPorCrm(int crm)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
}
