package Negocio;

import Basicas.*;
import DAO.RepoPaciente;

public class NegPaciente implements InterfacePaciente {

    @Override
    public void inserir(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente {
        RepoPaciente Repo = new RepoPaciente();
        //Validações
        Validacoes v = new Validacoes();
        if(p.getNome().isEmpty()){
            throw new Excecoes.CampoVazio("Nome Obrigatório");
        }else if(p.getCpf().isEmpty()){
            throw new Excecoes.CampoVazio("Cpf Obrigatório");
        }else if(Repo.buscarPorCpf(p.getCpf()) != null) {
            throw new Excecoes.RegistroExistente("Paciente já cadastrado.");
        }else if(v.nomeInvalido(p.getNome())){
            throw new Excecoes.DadoInvalido("Nome inválido");
        }else if(v.cpfInvalido(p.getCpf())){
            throw new Excecoes.DadoInvalido("Cpf inválido");
        }else {
            Repo.inserir(p);
            Repo.exibir(p);
            //Repo.finalizarSessao();
        }
    }

    @Override
    public void alterar(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente {
        if(this.buscarPorCpf(p.getCpf()) != null){
            RepoPaciente Repo = new RepoPaciente();
            //Validações
            Validacoes v = new Validacoes();
            if(p.getNome().isEmpty()){
                throw new Excecoes.CampoVazio("Nome Obrigatório");
            }else if(p.getCpf().isEmpty()){
                throw new Excecoes.CampoVazio("Cpf Obrigatório");
            }else if(Repo.buscarPorCpf(p.getCpf()) != null) {
                throw new Excecoes.RegistroExistente("Paciente já cadastrado.");
            }else if(v.nomeInvalido(p.getNome())){
                throw new Excecoes.DadoInvalido("Nome inválido");
            }else if(v.cpfInvalido(p.getCpf())){
                throw new Excecoes.DadoInvalido("Cpf inválido");
            }else {
                Repo.alterar(p);
                Repo.exibir(p);
                //Repo.finalizarSessao();
            }
        }else{
            throw new Excecoes.DadoInvalido("Paciente não existe");
        }

    }

    @Override
    public void remover(String cpf) {
        RepoPaciente Repo = new RepoPaciente();
        Repo.remover(cpf);
    }

    @Override
    public Paciente buscarPorNome(String nome) {
        RepoPaciente Repo = new RepoPaciente();
        return Repo.buscarPorCpf(nome);
    }

    @Override
    public Paciente buscarPorCpf(String cpf) {
        RepoPaciente Repo = new RepoPaciente();
        return Repo.buscarPorCpf(cpf);
    }

    @Override
    public Paciente buscarPorCodigo(int codigo) {
        RepoPaciente Repo = new RepoPaciente();
        return Repo.buscarPorCodigo(codigo);
    }

    @Override
    public void historico(String cpf) {
        throw new UnsupportedOperationException("Não implantado ainda."); 
    }
}