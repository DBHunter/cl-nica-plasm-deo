package Negocio;

import Basica.Usuario;
import DAO.RepoLogin;

public class NegLogin {
    
    public Usuario buscarUsuario(String login){
        RepoLogin repo = new RepoLogin();
            return repo.buscarUsuario(login);
    }
    
    public void salvarUsuario(String login, String senha){
        RepoLogin repo = new RepoLogin();
            repo.salvarUsuario(login, senha);
    }
    
    public void alterarSenha(String login, String senha){
        RepoLogin repo = new RepoLogin();
            repo.alterarSenha(login, senha);
    }
    
    public void removerUsuario(String login){
        RepoLogin repo = new RepoLogin();
            repo.removerUsuario(login);
    }
}
