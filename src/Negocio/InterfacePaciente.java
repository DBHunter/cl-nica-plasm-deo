package Negocio;
import Basicas.Excecoes;
import Basicas.Paciente;

public interface InterfacePaciente {
    
    public void inserir(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente;
    public void alterar(Paciente p) throws Excecoes.CampoVazio, Excecoes.DadoInvalido, Excecoes.RegistroExistente;
    public void remover(String cpf);
    public void historico(String cpf);
    public Paciente buscarPorNome(String nome)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
    public Paciente buscarPorCpf(String cpf)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
    public Paciente buscarPorCodigo(int codigo)throws Excecoes.CampoVazio, Excecoes.RegistroInexistente;
}
