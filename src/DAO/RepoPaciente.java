package DAO;
import ConexaoMysql.ConexaoMysql;
import Basicas.Paciente;
import java.util.ArrayList;

public class RepoPaciente{
    private static ArrayList<Paciente> RepoPacientes = new ArrayList<Paciente>();
    private static int codPaciente = 0;
            
    public int inserir(Paciente p) {
        RepoPacientes.add(p);
        codPaciente++;
        return -1;
    }

    public Paciente buscarPorNome(String nome) {
        Paciente p = new Paciente();
        for(int i=0; i<RepoPacientes.size();i++){
            if(RepoPacientes.get(i).getNome().equals(nome)){
                return RepoPacientes.get(i);
            }
        }
        return null;
    }

    public Paciente buscarPorCpf(String cpf) {
        Paciente p = new Paciente();
        for(int i=0; i<RepoPacientes.size();i++){
            if(RepoPacientes.get(i).getCpf().equals(cpf)){
                return RepoPacientes.get(i);
            }
        }
        return null;
    }

    public Paciente buscarPorCodigo(int codigo) {
        Paciente p = new Paciente();
        for(int i=0; i<RepoPacientes.size();i++){
            if(RepoPacientes.get(i).getCodigo() == codigo){
                return RepoPacientes.get(i);
            }
        }
        return null;
    }

    public int alterar(Paciente p) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }

    public int remover(String cpf) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }
    
    public void exibir(Paciente p) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }
    
    public void finalizarSessao(){
        //Inserir código - Salvar no Banco
        RepoPacientes.clear();
    }
}
