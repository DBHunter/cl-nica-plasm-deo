package DAO;
import ConexaoMysql.ConexaoMysql;
import Basicas.Medico;
import java.util.ArrayList;

public class RepoMedico{
    private static ArrayList<Medico> RepoMedicos = new ArrayList<Medico>();
    private static int codMedico = 0;
            
    public int inserir(Medico p) {
        RepoMedicos.add(p);
        codMedico++;
        return -1;
    }

    public Medico buscarPorNome(String nome) {
        Medico p = new Medico();
        for(int i=0; i<RepoMedicos.size();i++){
            if(RepoMedicos.get(i).getNome().equals(nome)){
                return RepoMedicos.get(i);
            }
        }
        return null;
    }

    public Medico buscarPorCpf(String cpf) {
        Medico p = new Medico();
        for(int i=0; i<RepoMedicos.size();i++){
            if(RepoMedicos.get(i).getCpf().equals(cpf)){
                return RepoMedicos.get(i);
            }
        }
        return null;
    }

    public Medico buscarPorCrm(int crm) {
        Medico p = new Medico();
        for(int i=0; i<RepoMedicos.size();i++){
            if(RepoMedicos.get(i).getCrm() == crm){
                return RepoMedicos.get(i);
            }
        }
        return null;
    }

    public int alterar(Medico p) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }

    public int remover(String cpf) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }
    
    public void exibir(Medico p) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Função inativa.");
    }
    
    public void finalizarSessao(){
        //Inserir código - Salvar no Banco
        RepoMedicos.clear();
    }
}
